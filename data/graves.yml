name: Graves-CPA
input_languages:
  - C
project_url: https://github.com/will-leeson/cpachecker
repository_url: https://github.com/will-leeson/cpachecker
spdx_license_identifier: Apache-2.0
benchexec_toolinfo_module: benchexec.tools.graves
fmtools_format_version: "2.0"
fmtools_entry_maintainers:
  - will-leeson

maintainers:
  - name: Will Leeson
    institution: University of Virginia
    country: USA
    url: https://will-leeson.github.io/

versions:
  - version: "svcomp24"
    doi: null
    benchexec_toolinfo_options: ['-svcomp23-graves', '-heap', '10000M', '-benchmark', '-timelimit', '900 s']
    required_ubuntu_packages:
      - openjdk-11-jre-headless
      - llvm-11
      - clang-11
  - version: "svcomp23"
    url: "https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/raw/main/2023/graves.zip"
    benchexec_toolinfo_options: []
    required_ubuntu_packages:
      - openjdk-11-jre-headless
      - llvm-11
      - clang-11

competition_participations:
  - competition: "SV-COMP 2024"
    track: "Verification"
    tool_version: "svcomp24"
    jury_member:
      name: Will Leeson
      institution: University of Virginia
      country: USA
      url: https://will-leeson.github.io/
  - competition: "SV-COMP 2023"
    track: "Verification"
    tool_version: "svcomp23"
    jury_member:
      name: Will Leeson
      institution: University of Virginia
      country: USA
      url: https://will-leeson.github.io/

techniques:
  - CEGAR
  - Predicate Abstraction
  - Bounded Model Checking
  - k-Induction
  - Explicit-Value Analysis
  - Numeric Interval Analysis
  - Shape Analysis
  - Bit-Precise Analysis
  - ARG-Based Analysis
  - Lazy Abstraction
  - Interpolation
  - Concurrency Support
  - Ranking Functions
  - Algorithm Selection
  - Portfolio
